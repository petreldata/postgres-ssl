# This Dockerfile contains the image specification of our database
FROM postgres:14.2

COPY ./certs/out/postgresdb.key /var/lib/postgresql
COPY ./certs/out/postgresdb.crt /var/lib/postgresql

COPY ./certs/out/petreldata.com.crt /var/lib/postgresql
COPY ./certs/out/petreldata.com.crl /var/lib/postgresql

RUN chown 0:postgres /var/lib/postgresql/postgresdb.key && chmod 640 /var/lib/postgresql/postgresdb.key
RUN chown 0:postgres /var/lib/postgresql/postgresdb.crt && chmod 640 /var/lib/postgresql/postgresdb.crt

RUN chown 0:postgres /var/lib/postgresql/petreldata.com.crt && chmod 640 /var/lib/postgresql/petreldata.com.crt
RUN chown 0:postgres /var/lib/postgresql/petreldata.com.crl && chmod 640 /var/lib/postgresql/petreldata.com.crl

ENTRYPOINT ["docker-entrypoint.sh"] 

CMD [ "-c", "ssl=on" , "-c", "ssl_cert_file=/var/lib/postgresql/postgresdb.crt", "-c",\
    "ssl_key_file=/var/lib/postgresql/postgresdb.key", "-c",\
    "ssl_ca_file=/var/lib/postgresql/petreldata.com.crt", "-c", "ssl_crl_file=/var/lib/postgresql/petreldata.com.crl" ]
